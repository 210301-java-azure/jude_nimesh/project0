drop table customer_list;
drop table order_list;
drop table product_items;
drop table user_list;

create table customer_list(
  	id serial primary key,
	cust_name varchar(50)  not null,
  	address varchar(80) not null,
  	email varchar(40) unique not null,
  	phone varchar(20) not null,
  	username varchar(20) unique not null,
  	password varchar(20) not null
);
------------------------------------------------------------------------------------------------
insert into customer_list (cust_name , address, email, phone, username, password ) 
values ('John Doe', '25 Main St. Reston, VA 20190', 'john.dove@gmail.com',
		 '703-555-1212','jdoe2', '112233');
insert into customer_list (cust_name , address, email, phone, username, password ) 
values ('Rajesh Manuel', '1098 Fairfax Rd. Chantily, VA 20163', 'rajesh.manuel@gmail.com',
		 '703-404-1123','rmanuel', '123456');
insert into customer_list (cust_name , address, email, phone, username, password ) 
values ('Prakash Joseph', '1587 ashburn Ave. South Reading, VA 20178', 'pjoseph@gmail.com',
		 '703-555-1456','jdoe1', '112233');		 
		
insert into customer_list (cust_name , address, email, phone, username, password ) 
values ('Jeba Seva', '4589 ashburn Ave. Herndon, VA 20172', 'Jeba.selva@gmail.com',
		 '703-555-5478','jselba', 'pasw0rd');	

insert into customer_list (cust_name , address, email, phone, username, password ) 
values ('Mike Kirsch', '44330 Woodridge Pkwy. Lessburg, VA 20176', 'mkirsch@gmail.com',
		 '301-478-6748','mkirsch', 'TeaPot18');		 
		
insert into customer_list (cust_name , address, email, phone, username, password ) 
values ('John Benedic', '1245 Lowes Ave. Fiarfax, VA 20163', 'jbenadic@gmail.com',
		 '404-258-1212','jbenadic', 'keyPsa*');		
------------------------------------------------------------------------------------------------
select * from customer_list;

insert into (cust_name , address, email, phone, username, password ) values (?, ?, ?, ?, ?, ?);

------------------------------------------------------------------------------------------------
drop table customer_list;
drop table order_list;
drop table product_items;
drop table user_list;

create table user_list(
  	username varchar(20) unique not null references customer_list (username),
  	password varchar(20) not null references customer_list (password)
);
------------------------------------------------------------------------------------------------  
create table product_items(
	  id serial primary key,
	  prod_name varchar(50) not null,
	  price numeric (6,2) not null,
	  stock numeric not null,
	  deprt_name varchar(50)
);
------------------------------------------------------------------------------------------------	    
insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Guitar', 650.99, 30, 'Instruments');	 

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Electric Guitar', 1250.99, 10, 'Instruments');	

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Drums', 2500, 6, 'Instruments');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Electric Drums', 2500, 3, 'Instruments');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Analog Mixer', 610.89, 15, 'Audio');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Speakers', 320.95, 10, 'Audio');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Spot Beam', 550.95, 5, 'Lighting');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Digital Camera', 1100.95, 5, 'Video');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Spot Beam', 550.95, 5, 'Lighting');

insert into product_items (prod_name , price, stock, deprt_name ) 
values ('Video Controler', 600.95, 5, 'Video');	  
------------------------------------------------------------------------------------------------
select * from product_items where prod_id = ?

select * from product_items;
select * from product_items where prod_id = 1	   

insert into (prod_name , price, stock, deprt_name ) values (?, ?, ?, ?); 
------------------------------------------------------------------------------------------------	    

create table order_list(
	  order_id serial primary key,
	  order_name varchar(50),
	  qty   int,
	  total numeric (6,2),
 	  cust_id int references customer_list ( id),
	  prod_id int references product_items ( id)
);

------------------------------------------------------------------------------------------------	
insert into order_list ( order_name, cust_id, prod_id,  qty, total ) 
values ('Gift', 1,  5,  10, 0 );		

insert into order_list ( order_name, cust_id, prod_id,  qty, total ) 
values ('Custom Order', 1,  6,  5,  0);	
		
insert into order_list (order_name, cust_id, prod_id, qty, total) 
values ('Special Order', 4, 8, 10, 0);	

insert into order_list (order_name, cust_id, prod_id, qty, total) 
values ('Charity Order', 3, 3, 75, 0);	

insert into order_list (order_name, cust_id, prod_id, qty, total) 
values ('VTA Order', 4, 2, 10, 0);	

insert into order_list (order_name, cust_id, prod_id, qty, total) 
values ('Custom Order', 6, 7, 10, 0);	
------------------------------------------------------------------------------------------------	

select cust_name, email, address, prod_name, price, qty 
from customer_list, order_list, product_items 
where  customer_list.cust_id = order_list.cust_id 
and order_list.prod_id = product_items.prod_id;


select * from order_list;
select * from order_list where order_id = 12;

select * from customer_list;
select * from customer_list where cust_id = 3;

insert into order_list (order_name, cust_id, prod_id, qty, total) 
values (?, ?, ?, ?, ?);

-- *** How to insert other columns (customer_list and produt_items) through this query ? --


where  customer_list.id = order_list.cust_id 
and order_list.prod_id = product_items.id;