package dev.nimesh.controllers;

import dev.nimesh.models.UserList;
import dev.nimesh.services.UserService;

import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);
    private UserService service = new UserService();
    private UserList userList = new UserList();

    // this method handles GET /items
    public void handleGetUserRequest(Context ctx){

       // /items with no query params
       //ctx.result(data.getAllItems().toString());}
       logger.info("getting all items");
         ctx.json(service.getAll()); // json method converts object to JSON
    }

    public void handleGetUserByIdRequest(Context ctx) {
        String usernameStr = ctx.pathParam("username");

        UserList item2 = service.getUserListById(usernameStr);
        logger.info("600: Customer username: "+ usernameStr+"\n"+item2.toString());
        if (item2 == null) {
          //                ctx.status(404);
            logger.warn("no item present with username: " + usernameStr);
            throw new NotFoundResponse("No item found with provided username: " + usernameStr);
        } else {
            logger.info("getting item with username: " + usernameStr);
            ctx.json(item2);
        }
    }

    public void handlePostNewUser(Context ctx){
        // getting params from what would be a form submission (Content-Type: application/x-www-form-urlencoded)
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");


        logger.info(user +" attempted login");
        userList =  new UserList( user, pass);
        //UserList user = ctx.   bodyAsClass(UserList.class);
        logger.info("adding new username: "+user);
        service.add(userList);

        //service.add(user);
        ctx.status(201);
    }

    public void handleDeleteUserById(Context ctx){
        String username = ctx.pathParam("username");
        logger.info("deleting record with username: "+username);
        service.delete(username);
    }

    public void handleUpdateUserById(Context ctx) {
        UserList newUser = ctx.bodyAsClass(UserList.class);
        logger.info("replacing a new username: "+newUser);

        String usernameStr = ctx.pathParam("username");

        logger.info("checking id got captured in int username: " + usernameStr);
        UserList user = service.getUserListById(usernameStr);
        if (user == null) {
            // ctx.status(404);
            logger.warn("no user present with username: " + usernameStr);
            throw new NotFoundResponse("No item found with provided ID: " + usernameStr);
        } else {
            logger.info("Before updating an existing user: "+user);

            service.update(user, newUser);
            logger.info("After Updating to a new user(on same index): "+newUser);
            ctx.json(newUser);
            ctx.status(201);
        }
    }

}
