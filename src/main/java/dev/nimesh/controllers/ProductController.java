package dev.nimesh.controllers;

import dev.nimesh.data.MusicProductDao;
import dev.nimesh.data.MusicProductData;
import dev.nimesh.models.MusicProduct;
import dev.nimesh.services.MusicStoreService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProductController {

    private Logger logger = LoggerFactory.getLogger(ProductController.class);
    private MusicStoreService service = new MusicStoreService();

    // this method handles GET /items
    public void handleGetProductRequest(Context ctx){
        logger.info("Product Items all listing");

        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        if(maxPrice!=null || minPrice!=null){
            logger.info("getting items in price range");
            ctx.json(service.getItemsInRange(minPrice, maxPrice));
        } else { // /items with no query params
            //ctx.result(data.getAllItems().toString());}
            logger.info("getting all items");
            ctx.json(service.getAll()); // json method converts object to JSON
        }
    }

    public void handleGetProductByIdRequest(Context ctx) {
        String idString = ctx.pathParam("id");

        logger.info("Product Items idString="+idString);
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            MusicProduct item = service.getById(idInput);
            if (item == null) {
                //                ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("getting item with id: " + idInput);
                ctx.json(item);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handlePostNewProduct(Context ctx){
        MusicProduct item = ctx.bodyAsClass(MusicProduct.class);
        logger.info("adding new item: "+item);
        service.add(item);
        ctx.status(201);
    }

    public void handleDeleteProductById(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: "+idInput);
            service.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void handleUpdateProductById(Context ctx) {
        MusicProduct newItem = ctx.bodyAsClass(MusicProduct.class);
        logger.info("replacing a new item: "+newItem);

        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("checking id got captured in int id: " + idInput); //test

            MusicProduct item = service.getById(idInput);
            if (item == null) {
                // ctx.status(404);
                logger.warn("no item present with id: " + idInput);
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                logger.info("Before updating an existing item: "+item);

                service.update(item, newItem);
                logger.info("After Updating to a new item(on same index): "+newItem);
                ctx.json(newItem);
                ctx.status(201);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

}
