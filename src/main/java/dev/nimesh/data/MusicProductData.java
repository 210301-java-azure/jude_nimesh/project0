package dev.nimesh.data;

import dev.nimesh.models.MusicProduct;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
    this was our original data access method with collections handling in memory items
 */

public class MusicProductData implements MusicProductDao {

    private List<MusicProduct> items = new ArrayList<>();
    Logger logger = LoggerFactory.getLogger(MusicProductData.class);

    public MusicProductData(){
        super();
        items.add(new MusicProduct(10,"Guitar","Instrument", 650, 24));
        items.add(new MusicProduct(5,"Camera","Video", 875.90, 10));
        items.add(new MusicProduct(10,"Mixer","Audio", 120, 5));
        items.add(new MusicProduct(10,"Spot Beam","Lighting", 1625.45, 3));

    }
    public List<MusicProduct> getAllProducts(){
        return new ArrayList<>(items);
    }

    public MusicProduct getProductById(int id){
        return items.stream().filter(item->item.getProdId()==id).findAny().orElse(null);
    }

    public MusicProduct addNewProduct(MusicProduct item){
        items.add(item);
        return item;
    }

    public void deleteProduct(int id){
//      items.removeIf(musicProduct -> (musicProduct!=null)?id==musicProduct.getId():false);
        Predicate<MusicProduct> idCheck = musicProduct -> musicProduct!=null && id==musicProduct.getProdId();
        items.removeIf(idCheck);
        // this remove operation can also be handled using iteration, looping through and using the
        // list remove method
    }
    public MusicProduct updateProduct(MusicProduct item, MusicProduct newItem) {

        MusicProduct oldItem = items.stream().filter(musicProduct -> musicProduct.getProdId() == item.getProdId()).findAny().orElse(null);
        logger.info("Before Updating to an old item: " + oldItem);
        logger.info("The new item replace on the indexOf(oldItem)=" + items.indexOf(oldItem) + "<-- new item: " + newItem);
        items.set(items.indexOf(oldItem), newItem);

        logger.info("After Updating to a new item: " + newItem);

        return newItem;  //
    }

    @Override
    public List<MusicProduct> getItemsInRange(double max, double min) { return null; }

    @Override
    public List<MusicProduct> getItemWithMaxPrice(double max) { return null; }

    @Override
    public List<MusicProduct> getItemWithMinPrice(double min) { return null; }

}


