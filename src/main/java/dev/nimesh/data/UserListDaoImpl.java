package dev.nimesh.data;

import dev.nimesh.models.UserList;
import dev.nimesh.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;

public class UserListDaoImpl implements  UserListDao {

    private Logger logger = LoggerFactory.getLogger(OrderListDaoImpl.class);

    @Override
    public ArrayList<UserList> getAllUsers() {
        ArrayList<UserList> users = new ArrayList<>();

        // try with resources - Connection is able to be used here because it implements Autocloseable
        try (Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from user_list");

            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                String username  = resultSet.getString("username");
                String password  = resultSet.getString("password");

                logger.info("User login info:  "+username+ " "+ password);
                UserList user = new UserList(username, password);
                users.add(user);
            }
            logger.info("selecting all users from db - "+ users.size()+ " users retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return users;
    }

    @Override
    public UserList getUserById(String username) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement prepareStatement = connection.prepareStatement("select * from user_list where username = ?");
            prepareStatement.setString(1,username);
            ResultSet resultSet = prepareStatement.executeQuery();
            if(resultSet.next()){
                String password  = resultSet.getString("password");

                logger.info("1 user retrieved from database by username: "+username+ ", password: "+password);
                System.out.println("Customer Info:"+ new UserList(username,password));

                return new UserList(username,password);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;
    }

    @Override
    public UserList addNewUser(UserList user) {
        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement =
                    connection.prepareStatement("insert into user_list ( username, password) values (?, ? )");

            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getPassword());

            preparedStatement.executeUpdate();
            logger.info("successfully added new user to the db");
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return user;
    }

    @Override
    public void deleteUser(String username) {
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from user_list where username = ?"); //can use name, price
            logger.info("Request to remove username: "+ username);
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                String password  = resultSet.getString("password");

                logger.info("1 user deleted from database by username: " +username+ ", password: "+password);
                new UserList(username, password);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
    }

    //@Override
    public UserList updateNewUser(UserList oldUser, UserList newUser) {
        try (Connection connection = ConnectionUtil.getConnection()) {

            String username  = newUser.getUsername();
            String password  = newUser.getPassword();

            PreparedStatement preparedStatement = connection.prepareStatement("select * from user_list where username = ?"); //can use name, price
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                String oldusername  = resultSet.getString("username");
                String oldpassword  = resultSet.getString("password");

                logger.info("1 user retrieved from database by username: " +username+ ", password: "+password);

                PreparedStatement preparedStatement2 =
                        connection.prepareStatement("update user_list set username = ?, password = ? " +
                                " where username = ?"); //can use name, price

                preparedStatement2.setString(1, username);
                preparedStatement2.setString(2, password);
                preparedStatement2.setString(3, username);

                preparedStatement2.executeUpdate();
                logger.info("successfully updated a new user to the db");
                logger.info("1 user updated to the database by username: "+username+ ", password: "+password);

                return new UserList(username,password);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + "  " + e.getMessage());
        }
        return null;
    }
}
