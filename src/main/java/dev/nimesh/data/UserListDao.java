package dev.nimesh.data;

import dev.nimesh.models.UserList;

import java.util.List;

public interface UserListDao {

    public List<UserList> getAllUsers();

    public UserList getUserById(String username);

    public UserList addNewUser(UserList users);

    void deleteUser(String username);

    public UserList updateNewUser(UserList oldItem, UserList newItem);
}
