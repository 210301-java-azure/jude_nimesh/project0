package dev.nimesh.data;

import dev.nimesh.models.MusicProduct;

import java.util.List;

public interface MusicProductDao {

    public List<MusicProduct> getAllProducts();
    public MusicProduct getProductById(int id);
    public MusicProduct addNewProduct(MusicProduct item);
    void deleteProduct(int id);

    public MusicProduct updateProduct(MusicProduct oldItem, MusicProduct newItem);

    public List<MusicProduct> getItemsInRange(double min, double max);
    public List<MusicProduct> getItemWithMaxPrice( double maxPrice);
    public List<MusicProduct> getItemWithMinPrice( double minPrice);


}
