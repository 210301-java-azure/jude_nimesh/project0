package dev.nimesh.data;

import dev.nimesh.models.CustomerList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/*
    this was our original data access method with collections handling in memory items
 */

public class CustomerListData implements CustomerListDao {

    private List<CustomerList> users = new ArrayList<>();
    Logger logger = LoggerFactory.getLogger(CustomerListData.class);

    public CustomerListData(){
        super();
        /*
        users.add(new CustomerList(10,"Guitar","Instrument", 650, 24));
        users.add(new CustomerList(5,"Camera","Video", 875.90, 10));
        users.add(new CustomerList(10,"Mixer","Audio", 120, 5));
        users.add(new CustomerList(10,"Spot Beam","Lighting", 1625.45, 3));
*/
    }
    public List<CustomerList> getAllCustomers(){
        return new ArrayList<>(users);
    }

    public CustomerList getCustomerById(int cust_id){
        /*
        for(CustomerList user : users){
            if(cust_id==user.getId()){
                return user;
            }
        }
        return null;
         */
        return users.stream().filter(user->user.getCustomerId()==cust_id).findAny().orElse(null);
    }

    public CustomerList addNewCustomer(CustomerList user){
        users.add(user);
        return user;
    }

    public void deleteCustomer(int cust_id){
//      items.removeIf(customerList -> (customerList!=null)?id==customerList.getId():false);
        Predicate<CustomerList> idCheck = customerList -> customerList!=null && cust_id==customerList.getCustomerId();
        users.removeIf(idCheck);
        // this remove operation can also be handled using iteration, looping through and using the
        // list remove method
    }
    public CustomerList updateCustomer(CustomerList user, CustomerList newUser) {

        CustomerList oldUser = users.stream().filter(customerList -> customerList.getCustomerId() == user.getCustomerId()).findAny().orElse(null);
        logger.info("Before Updating to an old item: " + oldUser);
        logger.info("The new item replace on the indexOf(oldUser)=" + users.indexOf(oldUser) + "<-- new user: " + newUser);
        users.set(users.indexOf(oldUser), newUser);

        logger.info("After Updating to a new item: " + newUser);

        return newUser;  //
    }
}


