package dev.nimesh.models;

import java.io.Serializable;
import java.util.Objects;

public class CustomerList  implements Serializable {

    private int cust_id;
    private String cust_name;
    private String address;
    private String email;
    private String phone;
    private String username;

    public CustomerList(){
        super();
    }

    public CustomerList(int cust_id, String cust_name, String address,
                        String email, String phone, String username) {
        this.cust_id   = cust_id;
        this.cust_name = cust_name;
        this.address   = address;
        this.email     = email;
        this.phone     = phone;
        this.username  = username;
    }


    public CustomerList(int cust_id) {
        this.cust_id = cust_id;
    }
    public CustomerList(String cust_name) {
        this.cust_name = cust_name;
    }

    public int getCustomerId() {
        return cust_id;
    }

    public void setCustomerId(int cust_id) {
        this.cust_id = cust_id;
    }

    public String getCustomerName() {
        return cust_name;
    }

    public void setCustomerName(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //public String getPassword() { return password; }
    //public void setPassword(String password) { this.password = password; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerList that = (CustomerList) o;
        return cust_id == that.cust_id && Objects.equals(cust_name, that.cust_name) &&
                Objects.equals(address, that.address) && Objects.equals(email, that.email) &&
                Objects.equals(phone, that.phone) && Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cust_id, cust_name, address, email, phone, username);
    }

    @Override
    public String toString() {
        return "CustomerList{" +
               "cust_id=" + cust_id +
               ", cust_name='" + cust_name + '\'' +
               ", address='" + address + '\'' +
               ", email='" + email + '\'' +
               ", phone='" + phone + '\'' +
               ", username='" + username + "\n"+'\'' +
               '}';
    }
}

