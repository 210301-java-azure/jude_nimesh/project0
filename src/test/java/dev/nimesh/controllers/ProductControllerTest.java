package dev.nimesh.controllers;



import dev.nimesh.models.MusicProduct;
import dev.nimesh.services.MusicStoreService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class ProductControllerTest {

    @InjectMocks
    private ProductController productController;

    /*
    We created a mock service here, a dummy object which we can give behavior by stubbing its methods
    We could also create a spy, which is a real object whose methods can be stubbed and invoked instead of the real methods
     */
    @Mock
    private MusicStoreService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllItemsHandler(){
        Context context = mock(Context.class);

        //int prod_id,  String prod_name, String deprt_name, double price, int stock ) {
        List<MusicProduct> items = new ArrayList<>();
        items.add(new MusicProduct(6, "Speakers", "Audio", 320.95, 10));
        items.add(new MusicProduct(7, "Spot Beam", "Lighting", 550.95, 5));
        items.add(new MusicProduct(9, "Guitar", "Instruments", 650.99, 30));

        when(service.getAll()).thenReturn(items);
        productController.handleGetProductByIdRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRangeWithMin(){
        Context context = mock(Context.class);

        List<MusicProduct> items = new ArrayList<>();
        items.add(new MusicProduct(7, "Spot Beam", "Lighting", 550.95, 5));
        items.add(new MusicProduct(9, "Guitar", "Instruments", 650.99, 30));

        when(service.getItemsInRange("600", null)).thenReturn(items);
        when(context.queryParam("min-price")).thenReturn("600");

        productController.handleGetProductByIdRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRangeWithMax(){
        Context context = mock(Context.class);

        List<MusicProduct> items = new ArrayList<>();
        items.add(new MusicProduct(6, "Speakers", "Audio", 620.95, 10));
        items.add(new MusicProduct(7, "Spot Beam", "Lighting", 550.95, 5));

        when(service.getItemsInRange(null, "775")).thenReturn(items);
        when(context.queryParam("max-price")).thenReturn("775");

        productController.handleGetProductByIdRequest(context);
        verify(context).json(items);
    }

    @Test
    public void testGetItemsInRange(){
        Context context = mock(Context.class);

        List<MusicProduct> items = new ArrayList<>();
        items.add(new MusicProduct(7, "Spot Beam", "Lighting", 550.95, 5));

        when(service.getItemsInRange("560", "750")).thenReturn(items);
        when(context.queryParam("min-price")).thenReturn("520");
        when(context.queryParam("max-price")).thenReturn("750");

        productController.handleGetProductByIdRequest(context);
        verify(context).json(items);
    }
}
